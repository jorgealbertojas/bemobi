package com.example.jorge.mybemobi.Inteface;

import com.example.jorge.mybemobi.Model.Device;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by jorge on 04/10/2017.
 */

/**
 * Interface Device for use get information Device.
 */
public interface DeviceInterface {
        @POST("/questions")
        Call<Device> createDevice(@Body Device device);


}
