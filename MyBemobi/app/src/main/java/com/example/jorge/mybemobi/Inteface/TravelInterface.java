package com.example.jorge.mybemobi.Inteface;

import com.example.jorge.mybemobi.Model.ListWrapper;
import com.example.jorge.mybemobi.Model.Travel;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by jorge on 03/10/2017.
 */

/**
 * Interface Travel for use get information Travel.
 *
 */
public interface TravelInterface {

    @GET("/questions")
    Call<ListWrapper<Travel>> getTravel();

}
