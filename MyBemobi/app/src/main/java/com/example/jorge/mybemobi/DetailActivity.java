package com.example.jorge.mybemobi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.jorge.mybemobi.Utilite.Utilite;
import com.squareup.picasso.Picasso;

/**
 * Class the detail the travel.
 */

public class DetailActivity extends AppCompatActivity {

    private String mImage;
    private String mTitle;
    private String mInformation;
    private String mValue;

    TextView tv_title;
    TextView tv_value;
    TextView tv_information;
    ImageView iv_imageTravel;

    /**
     * get all information the detail the travel.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle extras = getIntent().getExtras();
        mImage = extras.getString(Utilite.PUT_EXTRA_IMAGE);
        mTitle = extras.getString(Utilite.PUT_EXTRA_TITLE);
        mInformation = extras.getString(Utilite.PUT_EXTRA_INFORMATION);
        mValue = extras.getString(Utilite.PUT_EXTRA_VALUE);

        tv_title  = (TextView) findViewById(R.id.tv_title);
        tv_value  = (TextView) findViewById(R.id.tv_value);
        tv_information  = (TextView) findViewById(R.id.tv_information);
        iv_imageTravel  = (ImageView) findViewById(R.id.iv_imageTravel);

        tv_title.setText(mTitle);
        tv_value.setText(mValue);
        tv_information.setText(mInformation);
        Picasso.with(this).load(mImage).fit().into(iv_imageTravel);

    }

    /**
     * Share for other app.
     */
    private Intent createShareForecastIntent() {
        Intent shareIntent = ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setText("Place" + mTitle + "  Image: " + mImage)
                .getIntent();
        return shareIntent;
    }

    /**
     * Share for other app.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);
        MenuItem menuItem = menu.findItem(R.id.action_share);
        menuItem.setIntent(createShareForecastIntent());
        return true;
    }
}
