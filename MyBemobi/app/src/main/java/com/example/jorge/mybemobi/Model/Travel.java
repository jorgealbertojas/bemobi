package com.example.jorge.mybemobi.Model;

/**
 * Created by jorge on 03/10/2017.
 */
/**
 * Model full field travel.
 */
public class Travel {

    private int id;
    private String  nome_do_pacote;
    private String  valor;
    private String  foto_da_localizacao;
    private String  descricao_do_pacote;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome_do_pacote() {
        return nome_do_pacote;
    }

    public void setNome_do_pacote(String nome_do_pacote) {
        this.nome_do_pacote = nome_do_pacote;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getFoto_da_localizacao() {
        return foto_da_localizacao;
    }

    public void setFoto_da_localizacao(String foto_da_localizacao) {
        this.foto_da_localizacao = foto_da_localizacao;
    }

    public String getDescricao_do_pacote() {
        return descricao_do_pacote;
    }

    public void setDescricao_do_pacote(String descricao_do_pacote) {
        this.descricao_do_pacote = descricao_do_pacote;
    }



}
