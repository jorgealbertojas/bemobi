package com.example.jorge.mybemobi.Utilite;

/**
 * Created by jorge on 04/10/2017.
 */
/**
 * const for all system.
 */
public final class Utilite {

    public final static String BASE_URL = "https://private-ee5939-apibemobi.apiary-mock.com";

    /**
     * Field for PUT EXTRA.
     */
    public final static String PUT_EXTRA_TITLE = "TITLE";
    public final static String PUT_EXTRA_IMAGE = "IMAGE";
    public final static String PUT_EXTRA_VALUE = "VALUE";
    public final static String PUT_EXTRA_INFORMATION = "INFORMATION";
}