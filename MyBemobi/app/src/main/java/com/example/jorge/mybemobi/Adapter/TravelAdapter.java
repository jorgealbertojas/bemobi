package com.example.jorge.mybemobi.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jorge.mybemobi.Model.Travel;
import com.example.jorge.mybemobi.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TravelAdapter extends RecyclerView.Adapter<TravelAdapter.ViewHolder> {

    private List<Travel> data;

    private Context mContext;


    /*
 * An on-click handler that we've defined to make it easy for an Activity to interface with
 * our RecyclerView
 */
    private static TravelAdapterOnClickHandler mClickHandler;
    /**
     * The interface that receives onClick messages.
     */
    public interface TravelAdapterOnClickHandler {
        void onClick(Travel travel);
    }

    /** Constructs the class**/
    public  TravelAdapter(TravelAdapterOnClickHandler clickHandler) {
        mClickHandler = clickHandler;
    }


    /** class view holder**/
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final ImageView mMovieImageView;
        public final TextView mTitleTextView;
        public final TextView mValueTextView;

        /** get field of the main for show recyclerview**/
        public ViewHolder(View v) {
            super(v);
            mMovieImageView = (ImageView) v.findViewById(R.id.iv_imageTravel);
            mTitleTextView = (TextView) v.findViewById(R.id.tv_TitleTravel);
            mValueTextView = (TextView) v.findViewById(R.id.tv_ValueTravel);
            v.setOnClickListener(this);
        }

        /** configuration the Event onclick. Pass o Object Travel **/
        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            Travel travel = data.get(adapterPosition);
            mClickHandler.onClick(travel);

        }
    }

    /** create lit de Adapter Travel**/
    public TravelAdapter(List<Travel> data) {
        this.data = data;
    }

    /** Create information View holder**/
    @Override
    public TravelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.informatiom_travel, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v);
    }

    /** Create filed bind hold full **/
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Travel answer = ((Travel) data.get(position));
        holder.mTitleTextView.setText(answer.getNome_do_pacote());
        holder.mValueTextView.setText(answer.getValor());
        Picasso.with(mContext).load(answer.getFoto_da_localizacao()).into(holder.mMovieImageView);

    }

    /** Returns the total Adapter**/
    @Override
    public int getItemCount() {
        return data.size();
    }

}