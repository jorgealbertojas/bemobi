package com.example.jorge.mybemobi.Model;

/**
 * Created by jorge on 04/10/2017.
 */
/**
 * Model full field device.
 */
public class Device {

    private String model;
    private String company;
    private String sdk;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSdk() {
        return sdk;
    }

    public void setSdk(String sdk) {
        this.sdk = sdk;
    }



}
