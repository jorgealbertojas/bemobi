package com.example.jorge.mybemobi;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;
import com.example.jorge.mybemobi.Adapter.TravelAdapter;
import com.example.jorge.mybemobi.Adapter.TravelAdapter.TravelAdapterOnClickHandler;
import com.example.jorge.mybemobi.Inteface.DeviceInterface;
import com.example.jorge.mybemobi.Inteface.TravelInterface;
import com.example.jorge.mybemobi.Model.Device;
import com.example.jorge.mybemobi.Model.ListWrapper;
import com.example.jorge.mybemobi.Model.Travel;
import com.example.jorge.mybemobi.Utilite.Utilite;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Class Main list in RecyclerView all travel .
 */
public class MainActivity extends AppCompatActivity  implements TravelAdapterOnClickHandler {

    TravelAdapter mTravelAdapter;
    private TravelInterface mTravelInterface;
    private RecyclerView mRecyclerView;
    private DeviceInterface mDeviceInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_numbers);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        mTravelAdapter = new TravelAdapter(this);

        if (isOnline()) {
            createStackoverflowAPI();
            mTravelInterface.getTravel().enqueue(travelCallback);
            postDevicePI();
            ExcutePost();
        }else{
            Context contexto = getApplicationContext();
            Toast toast = Toast.makeText(contexto, R.string.Error_Access,Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    /**
     * Call Post Information Device .
     */
    private void ExcutePost() {
        String company = Build.MANUFACTURER;
        String model = Build.MODEL;
        String sdk = Build.VERSION.SDK;


        Device device = new Device();

        device.setCompany(company);
        device.setModel(model);
        device.setSdk(sdk);

        Call<Device> call = mDeviceInterface.createDevice(device);
        call.enqueue(new Callback<Device>() {
            @Override
            public void onResponse(Call<Device> call, Response<Device> response) {
                try {
                    String model = response.body().getModel();
                    String company = response.body().getCompany();
                    String sdk = response.body().getSdk();

                    Context contexto = getApplicationContext();
                    Toast toast = Toast.makeText(contexto, "Model:" + model + "  Company:" + company + "  SDK:" + sdk, Toast.LENGTH_SHORT);
                    toast.show();
                }
                    catch(NullPointerException e){

                        System.out.println("onActivityResult consume crashed");
                        runOnUiThread(new Runnable(){
                            public void run(){

                                Context contexto = getApplicationContext();
                                Toast toast = Toast.makeText(contexto, R.string.Error_Access_empty,Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        });
                    }
            }

            @Override
            public void onFailure(Call<Device> call, Throwable t) {
                Log.e("QuestionsCallback", "Code: " + t.getMessage() + " Message: " + t.getCause());
            }
        });

    }

    /**
     * Call Get Information Travel .
     */
    private Callback<ListWrapper<Travel>> travelCallback = new Callback<ListWrapper<Travel>>() {
        @Override
        public void onResponse(Call<ListWrapper<Travel>> call, Response<ListWrapper<Travel>> response) {
            try{
                if (response.isSuccessful()) {
                    List<Travel> data = new ArrayList<>();
                    data.addAll(response.body().items);
                    mRecyclerView.setAdapter(new TravelAdapter(data));

                } else {
                    Log.d("QuestionsCallback", "Code: " + response.code() + " Message: " + response.message());
                }
            }catch(NullPointerException e){
                System.out.println("onActivityResult consume crashed");
                runOnUiThread(new Runnable(){
                    public void run(){

                        Context contexto = getApplicationContext();
                        Toast toast = Toast.makeText(contexto, R.string.Error_Access_empty,Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
            }
        }

        @Override
        public void onFailure(Call<ListWrapper<Travel>> call, Throwable t) {
            t.printStackTrace();
        }
    };

    /**
     * Open connect with URL for get JSON  .
     */
    private void createStackoverflowAPI() {
        Gson gson = new GsonBuilder()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utilite.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mTravelInterface = retrofit.create(TravelInterface.class);
    }

    /**
     * Post connect with URL for salve JSON  in API.
     */
    private void postDevicePI() {
        Gson gson = new GsonBuilder()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utilite.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mDeviceInterface = retrofit.create(DeviceInterface.class);
    }

    /**
     * click for choose item the travel .
     */
    @Override
    public void onClick(Travel travel) {
        Context context = this;
        Class destinationClass = DetailActivity.class;
        Intent intentToStartDetailActivity = new Intent(context, destinationClass);
        intentToStartDetailActivity.putExtra(Utilite.PUT_EXTRA_TITLE, travel.getNome_do_pacote());
        intentToStartDetailActivity.putExtra(Utilite.PUT_EXTRA_IMAGE, travel.getFoto_da_localizacao());
        intentToStartDetailActivity.putExtra(Utilite.PUT_EXTRA_INFORMATION, travel.getDescricao_do_pacote());
        intentToStartDetailActivity.putExtra(Utilite.PUT_EXTRA_VALUE, travel.getValor());
        startActivity(intentToStartDetailActivity);

    }


    /**
     * checks if internet is ok .
     */
    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


}
